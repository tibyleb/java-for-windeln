// ssh-keygen -t rsa
// cat .ssh/id_rsa.pub

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

public class TestSelenium {

    public static ChromeDriver driver=new ChromeDriver();
    //search for a given element in the page
    private static WebElement searchPage(ChromeDriver driver, String query, String elementType) {

        if ("id".equals(elementType)){
            return driver.findElementById(query);
        } else if ("xpath".equals(elementType)){
            return driver.findElementByXPath(query);
        } else {
            return driver.findElement(By.name(query));
        }
    }

    //click on any found element in the page
    private static void clickElement(ChromeDriver driver, String query, String elementType) {
        WebElement element=searchPage(driver, query, elementType);
        element.click();
    }

    //click on the account icon for further login or registration
    private static void goToLoginPage(ChromeDriver driver){
        clickElement(driver,"//header/div/div/div[3]/div[1]/div[3]/div[1]/a", "xpath"); // click on account icon
    }

    //test login with existing account and correct password
    private static void testNominalLogin(ChromeDriver driver, String email, String password) throws InterruptedException {
        System.out.println("[+] Testing login: correct account vs. correct password");
        goToLoginPage(driver);
        searchPage(driver,"loginEmail", "id").sendKeys(email); // types the email account
        searchPage(driver,"loginPassword", "id").sendKeys(password); //types the password
        clickElement(driver,"//*[@id=\"loginForm\"]/div/div[2]/input", "xpath"); // clicks on login button

        if (driver.getCurrentUrl().equals("https://stage.windeln.de/account/")) {
            System.out.println("\tSuccess\n");
        }
        else{
            System.out.println("\tFailled\n");
        }
        logOut(driver);
    }

    //test login with existing account and wrong password
    private static void testValidationLoginWrongPassword(ChromeDriver driver, String email, String password) {
        System.out.println("\n[+] Testing login: correct account vs. incorrect password");
        goToLoginPage(driver);
        searchPage(driver,"loginEmail", "id").sendKeys(email); // types the email account
        searchPage(driver,"loginPassword", "id").sendKeys(password); //types the password
        clickElement(driver,"//*[@id=\"loginForm\"]/div/div[2]/input", "xpath");

        if (searchPage(driver,"//*[@id='messages']/div", "xpath").isDisplayed()) {
            System.out.println("\tSuccess\n");
        }
        else{
            System.out.println("\tFailled\n");
        }
    }

    //logs out after successful login or new account creation
    private static void logOut(ChromeDriver driver) throws InterruptedException {
        System.out.println("\t[+] User is being logged out");
        Actions action = new Actions(driver);
        action.moveToElement(searchPage(driver, "//*[@id=\"pageId-account\"]/header/div/div/div[3]/div[1]/div[3]/div[1]", "xpath")).perform();
        TimeUnit.SECONDS.sleep(2);
        action.moveToElement(searchPage(driver, "//*[@id=\"pageId-account\"]/header/div/div/div[3]/div[1]/div[3]/div[1]/div/form/input[2]", "xpath")).click().perform();
        if (driver.getCurrentUrl().equals("https://stage.windeln.de/")){
            System.out.println("\t\tSuccess\n");
        }else{
            System.out.println("\t\tFailed\n");
        }
    }

    //creates new account with valid email address generated in http://10minutemail.net + valid email
    private static void testNominalCreateUserWithNL_Subs(ChromeDriver driver, String email, String password) throws InterruptedException {
        System.out.println("[+] Testing new user: correct account vs. correct password");

        goToLoginPage(driver);
        searchPage(driver, "registerEmail", "id").sendKeys(email);
        searchPage(driver, "registerPassword", "id").sendKeys(password);
        searchPage(driver, "registerPasswordConfirm", "id").sendKeys(password);
        clickElement(driver, "windeln_de_newsletter", "id");
        clickElement(driver, ".//div[@class='btn-loud']", "xpath");

        if ("https://stage.windeln.de/account/".equals(driver.getCurrentUrl())) {
            System.out.println("\tSuccess\n");
        } else {
            System.out.println("\tFailed\n");
        }
        logOut(driver);
    }

    //generate new email from http://10minutemail.net
    private static String createTempEmail(ChromeDriver driver) throws InterruptedException {
        driver.executeScript("window.open('"+propertyManager.getInstance().getUrl10min()+"');");
        TimeUnit.SECONDS.sleep(5);
        ArrayList<String> tabs = new ArrayList<>(driver.getWindowHandles());
        driver.switchTo().window(tabs.get(1));
        String email = searchPage(driver, "//*[@id='fe_text']","xpath").getAttribute("value");
        driver.switchTo().window(tabs.get(0));
        return email;
    }

    public static void main(String[] args) throws InterruptedException {

        //variables definition
        String email = propertyManager.getInstance().getExistingAccount();
        String password = propertyManager.getInstance().getCorrectPassword();
        String wrongPassword = propertyManager.getInstance().getWrongPassword();
        String newUserPassword = propertyManager.getInstance().getNewUserPassword();

        // open stage.windeln.de in maximized Chrome window and clicks on the TC button



        if (System.getProperty("os.name").contains("Windows")) {
            driver.manage().window().maximize();
        }else {
            driver.manage().window().fullscreen();
        }
        testFilters.testSelectRandomFilter(driver);
//        driver.get(propertyManager.getInstance().getURL());
//        driver.findElementById("footer_tc_privacy_button").click();
//
//        //start calling the tests
//        if (propertyManager.getInstance().getTestNominalLogin()== Boolean.TRUE) {
//            testNominalLogin(driver, email, password);
//        }else{
//            System.out.println("TestNominalLogin was not selected");
//        }
//        if (propertyManager.getInstance().getTestValidationLoginWrongPassword()==Boolean.TRUE) {
//            testValidationLoginWrongPassword(driver, email, wrongPassword);
//        }else{
//            System.out.println("TestValidationLoginWrongPassword was not selected");
//        }
//        if (propertyManager.getInstance().getTestNominalCreateUserWithNL_Subs()==Boolean.TRUE) {
//            testNominalCreateUserWithNL_Subs(driver, createTempEmail(driver), newUserPassword);
//        }else{
//            System.out.println("testNominalCreateUserWithNL_Subs was not selected");
//        }
        driver.quit();

    }

}
