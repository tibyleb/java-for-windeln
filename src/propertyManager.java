import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

//Description: PropertyManager class reads global configurations and use them during test execution.
public class propertyManager {

    private static propertyManager instance;
    private static final Object lock = new Object();
    private static String propertyFilePath() {
        if (System.getProperty("os.name").contains("Windows")){
            return System.getProperty("user.dir") + "\\src\\config.properties";
        }else{
            return System.getProperty("user.dir") + "/src/config.properties";
        }
    }
    private static String url;
    private static String existingAccount;
    private static String wrongPassword;
    private static String correctPassword;
    private static String url10min;
    private static String newUserPassword;
    private static Boolean testNominalLogin;
    private static Boolean testValidationLoginWrongPassword;
    private static Boolean testNominalCreateUserWithNL_Subs;


    //Create a Singleton instance. We need only one instance of Property Manager.
    public static propertyManager getInstance () {
        if (instance == null) {
            synchronized (lock) {
                instance = new propertyManager();
                instance.loadData();
            }
        }
        return instance;
    }

    //Get all configuration data and assign to related fields.
    private void loadData() {

        //Declare a properties object
        Properties prop = new Properties();

        //Read configuration.properties file
        try {
            prop.load(new FileInputStream(propertyFilePath()));
        } catch (IOException e) {
            System.out.println("Configuration properties file cannot be found");
        }

        //Get properties from configuration.properties
        url = prop.getProperty("url");
        url10min = prop.getProperty("url10min");
        existingAccount = prop.getProperty("existingAccount");
        wrongPassword = prop.getProperty("wrongPassword");
        correctPassword = prop.getProperty("correctPassword");
        newUserPassword = prop.getProperty("newUserPassword");
        testNominalLogin = Boolean.valueOf(prop.getProperty("testNominalLogin"));
        testValidationLoginWrongPassword = Boolean.valueOf(prop.getProperty("testValidationLoginWrongPassword"));
        testNominalCreateUserWithNL_Subs = Boolean.valueOf(prop.getProperty("testNominalCreateUserWithNL_Subs"));
    }

    public String getURL () { return url; }
    public String getExistingAccount () {return existingAccount;}
    public String getWrongPassword () {return wrongPassword;}
    public String getCorrectPassword (){return  correctPassword; }
    public String getUrl10min (){return url10min; }
    public String getNewUserPassword(){ return newUserPassword; }
    public Boolean getTestNominalLogin (){ return testNominalLogin; }
    public Boolean getTestValidationLoginWrongPassword (){ return testValidationLoginWrongPassword; }
    public Boolean getTestNominalCreateUserWithNL_Subs (){ return testNominalCreateUserWithNL_Subs; }
}

